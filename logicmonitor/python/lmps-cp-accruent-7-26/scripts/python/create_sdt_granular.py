"""
This script creates either a device, device datasource, or device datasource
instance scheduled down time (SDT) via LogicMonitor's REST API.

usage: create_sdt_granular.py [-h] -c COMPANY_NAME -u ACCESS_ID -p API_KEY -n
                              NAME_FILTER [-m LOGICMODULE_FILTER]
                              [-i INSTANCE_FILTER] -r COMMENT [-s START_TIME]
                              [-t STOP_TIME] -l [LOG]

optional arguments:
  -h, --help            show this help message and exit
  -c COMPANY_NAME, --company-name COMPANY_NAME
                        LogicMonitor customer portal.
  -u ACCESS_ID, --access-id ACCESS_ID
                        LogicMonitor API token access id.
  -p API_KEY, --api-key API_KEY
                        LogicMonitor API token access key.
  -o OBJECT_ID, --object-id OBJECT_ID
                        Device or service id.
  -m LOGICMODULE_FILTER, --logicmodule-filter LOGICMODULE_FILTER
                        LogicMonitor LogicModule filter
  -i INSTANCE_FILTER, --instance-filter INSTANCE_FILTER
                        LogicMonitor Instance filter
  -r COMMENT, --comment COMMENT
                        Maintenance window message for LogicMonitor SDT.
  -s START_TIME, --start-time START_TIME
                        Starting time for maintenance window in form "mm/dd/yy
                        HH:MM"
  -t STOP_TIME, --stop-time STOP_TIME
                        Stopping time for maintenance window in form "mm/dd/yy
                        HH:MM"
  -l [LOG], --log [LOG]
                        Log path for saving script execution information.


Output (unless overridden by user parameters):
../../logs/create_sdt_granular.log
"""
import argparse
import logging
import sys
import os
from logic_monitor_client import LogicMonitorClient, EpochConverter

FILENAME = os.path.basename(sys.argv[0])
LOGPATH = '../../logs/' + FILENAME.replace('.py', '.log')


def execute(company, access_id, access_key,
            object_id, logicmodule_filter, instance_filter,
            start_time, stop_time, comment):
    """
    Gets all alerts by filter and writes to csv.
    :param company: portal name
    :param access_id: token access id
    :param access_key: token access key
    :param name_filter: string filter for specifying device/service
    :param logicmodule_filter: string filter for specifying logicmodule
    :param instance_filter: string filter for specifying instance 
    :param start_time: string datetime in "mm/dd/yy HH:MM" format
    :param end_time: string datetime in "mm/dd/yy HH:MM" format
    :param comment: string comment for sdt
    :return: 
    """
    logic_monitor_client = LogicMonitorClient(company, access_id, access_key)

    start_time = EpochConverter.convert_to_epoch(start_time) * 1000
    stop_time = EpochConverter.convert_to_epoch(stop_time) * 1000

    if instance_filter and logicmodule_filter:
        logicmodules = logic_monitor_client.get_device_logicmodules(object_id,
                                                                   logicmodule_filter)

        for logicmodule in logicmodules:
            instances = logic_monitor_client.get_device_logicmodule_instances(object_id,
                                                                              logicmodule['id'],
                                                                              instance_filter)
            for instance in instances:
                response = logic_monitor_client.set_device_logicmodule_instance_sdt(object_id,
                                                                                    instance['id'],
                                                                                    start_time,
                                                                                    stop_time,
                                                                                    comment)
                logging.info('Attempted to set a Device Logicmodule Instance '
                             'SDT. Response:')
                logging.info(response)

    elif logicmodule_filter and not instance_filter:
        logicmodules = logic_monitor_client.get_device_logicmodules(object_id,
                                                                    logicmodule_filter)

        for logicmodule in logicmodules:
            response = logic_monitor_client.set_device_logicmodule_sdt(object_id,
                                                                       logicmodule['id'],
                                                                       start_time,
                                                                       stop_time,
                                                                       comment)
            logging.info('Attempted to set a Device Logicmodule SDT. Response:')
            logging.info(response)

    elif not logicmodule_filter and not instance_filter:
        response = logic_monitor_client.set_device_sdt(object_id, start_time,
                                                       stop_time, comment)
        logging.info('Attempted to set a Device SDT. Response:')
        logging.info(response)
    elif not logicmodule_filter and instance_filter:
        logging.error('Must specify a logicmodule filter if specifying an '
                      'instance filter.')


def main():
    """
    Main method
    :return: None
    """
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--company-name', type=str,
                            help='LogicMonitor customer portal.', required=True)
        parser.add_argument('-u', '--access-id', type=str,
                            help='LogicMonitor API token access id.',
                            required=True)
        parser.add_argument('-p', '--api-key', type=str,
                            help='LogicMonitor API token access key.',
                            required=True)
        parser.add_argument('-o', '--object-id', type=str,
                            help='Device or service ID.', required=True)
        parser.add_argument('-m', '--logicmodule-filter', type=str,
                            help='LogicMonitor LogicModule filter', required=False)
        parser.add_argument('-i', '--instance-filter', type=str,
                            help='LogicMonitor Instance filter', required=False)
        parser.add_argument('-r', '--comment', type=str,
                            help='Maintenance window message for LogicMonitor SDT.',
                            required=True)
        parser.add_argument('-s', '--start-time', type=str,
                            help='Starting time for maintenance window in form "mm/dd/yy HH:MM"', required=True)
        parser.add_argument('-t', '--stop-time', type=str,
                            help='Stopping time for maintenance window in form "mm/dd/yy HH:MM"', required=True)
        parser.add_argument('-l', '--log', type=str, nargs='?', const=LOGPATH,
                            help='Log path for saving script execution information.',
                            required=False)
        args = parser.parse_args()

        logging.basicConfig(filename=args.log,
                            level=logging.INFO,
                            format='%(levelname)s:%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger().addHandler(logging.StreamHandler())
        logging.info('Executing {0} to set SDT...'.format(FILENAME))

        execute(args.company_name, args.access_id, args.api_key,
                args.object_id, args.logicmodule_filter, args.instance_filter,
                args.start_time, args.stop_time, args.comment)

        logging.info('Completed {0} execution.'.format(FILENAME))
    except Exception, e:
        error_msg = 'Exception:' + str(e) + ' Message:' + e.message
        logging.error(error_msg)
        logging.info('Failed to complete {0} execution.'.format(FILENAME))


main()
