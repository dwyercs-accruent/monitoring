"""
This script creates either a device group or service group
scheduled down time (SDT) via LogicMonitor's REST API.

usage: create_logicmonitor_sdt.py [-h] -c COMPANY_NAME -u ACCESS_ID -p API_KEY [-d] [-e] -i
              GROUP_ID -r COMMENT -s START_TIME -t STOP_TIME

optional arguments:
  -h, --help            show this help message and exit
  -c COMPANY_NAME, --company-name COMPANY_NAME
                        LogicMonitor customer portal.
  -u ACCESS_ID, --access-id ACCESS_ID
                        LogicMonitor API token access ID.
  -p API_KEY, --api-key API_KEY
                        LogicMonitor API token access key.
  -d, --device-group    Use a device group SDT. CANNOT be used simultaneously
                        as service group option.
  -e, --service-group   Use service group SDT. CANNOT be used simultaneously
                        as device group option.
  -i GROUP_ID, --group-id GROUP_ID
                        Either LogicMonitor device or service group ID,
                        depending on option (-d or -e) used.
  -r COMMENT, --comment COMMENT
                        Maintenance window message for LogicMonitor SDT.
  -s START_TIME, --start-time START_TIME
                        Starting datetime for maintenance window in form
                        "mm/dd/yy HH:MM".
  -t STOP_TIME, --stop-time STOP_TIME
                        Ending datetime for maintenance window in form
                        "mm/dd/yy HH:MM".
Output (unless overridden by user parameters):
../../logs/create_logicmonitor_sdt.log
"""
import argparse
import logging
import sys
import os
from logic_monitor_client import LogicMonitorClient, EpochConverter

FILENAME = os.path.basename(sys.argv[0])
LOGPATH = '../../logs/' + FILENAME.replace('.py', '.log')

def main():
    """
    Main method
    :return: None
    """
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--company-name', type=str,
                            help='LogicMonitor customer portal.', required=True)
        parser.add_argument('-u', '--access-id', type=str,
                            help='LogicMonitor API token access ID.', required=True)
        parser.add_argument('-p', '--api-key', type=str,
                            help='LogicMonitor API token access key.', required=True)
        parser.add_argument('-d', '--device-group',
                            action='store_true', help='Use a device group SDT. CANNOT be used simultaneously as service group option.')
        parser.add_argument('-e', '--service-group',
                            action='store_true', help='Use service group SDT. CANNOT be used simultaneously as device group option.')
        parser.add_argument('-i', '--group-id', type=str,
                            help='Either LogicMonitor device or service group ID, depending on option (-d or -e) used.',
                            required=True)
        parser.add_argument('-r', '--comment', type=str,
                            help='Maintenance window message for LogicMonitor SDT.',
                            required=True)
        parser.add_argument('-s', '--start-time', type=str,
                            help='Starting datetime for maintenance window in form "mm/dd/yy HH:MM".', required=True)
        parser.add_argument('-t', '--stop-time', type=str,
                            help='Ending datetime for maintenance window in form "mm/dd/yy HH:MM".', required=True)
        parser.add_argument('-l', '--log', type=str, nargs='?', const=LOGPATH,
                            help='Log path for saving script execution information.', required=True)
        args = parser.parse_args()

        logging.basicConfig(filename=args.log,
                            level=logging.INFO,
                            format='%(levelname)s:%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger().addHandler(logging.StreamHandler())
        logging.info('Executing {0} to create LogicMonitor SDT'.format(FILENAME))

        args.start_time = EpochConverter.convert_to_epoch(args.start_time) * 1000
        args.stop_time = EpochConverter.convert_to_epoch(args.stop_time) * 1000

        if args.device_group and args.service_group:
            logging.info("You cannot specify both a device group and service group SDT!")
        elif args.device_group:
            lm = LogicMonitorClient(args.company_name, args.access_id, args.api_key)
            response = lm.set_device_group_sdt(args.group_id, args.start_time, args.stop_time, args.comment)
            logging.info("Attempted to set a Device Group SDT. Response:")
            logging.info(response)
        elif args.service_group:
            lm = LogicMonitorClient(args.company_name, args.access_id, args.api_key)
            response = lm.set_service_group_sdt(args.group_id, args.start_time, args.stop_time, args.comment)
            logging.info("Attempted to set a Service Group SDT. Response:")
            logging.info(response)
        else:
            logging.info('Unknown maintainence type. Please choose device group or service group.')
        logging.info('Completed {0} execution.'.format(FILENAME))
        return
    except Exception, e:
        error_msg = 'Exception:' + str(e) + ' Message:' + e.message
        logging.error(error_msg)
        logging.info('Failed to complete {0} execution.'.format(FILENAME))
        return

if __name__ == '__main__':
    main()
