"""
This script adds custom properties to devices in a device group
via LogicMonitor's REST API. The custom properties must be listed
in a CSV file where each new line contains a comma delimited
key value pair where the key is the property name and value
is the property value:
key1,value1
key2,value2

usage: assign_custom_properties.py [-h] -c COMPANY_NAME -u ACCESS_ID -p
                                   API_KEY -g DEVICE_GROUPID
                                   [-d [SYSTEMDISPLAYNAME_REGEX]]
                                   [-e [SYSTEMDISPLAYNAME_CSV]]
                                   [-s [SYSTEMCATEGORIES_REGEX]]
                                   [-t [SYSTEMCATEGORIES_CSV]] [-m]
                                   [-n [BOTH_CSV]] -l [LOG]

optional arguments:
  -h, --help            show this help message and exit
  -c COMPANY_NAME, --company-name COMPANY_NAME
                        LogicMonitor customer portal.
  -u ACCESS_ID, --access-id ACCESS_ID
                        LogicMonitor API token access ID.
  -p API_KEY, --api-key API_KEY
                        LogicMonitor API token access key.
  -g DEVICE_GROUPID, --device-groupid DEVICE_GROUPID
                        LogicMonitor device group ID to search for devices.
  -d [SYSTEMDISPLAYNAME_REGEX], --systemdisplayname-regex [SYSTEMDISPLAYNAME_REGEX]
                        Regex for matching device display names in given
                        device group ID.
  -e [SYSTEMDISPLAYNAME_CSV], --systemdisplayname-csv [SYSTEMDISPLAYNAME_CSV]
                        Map of properties and values to assign to devices
                        matching display name regex.
  -s [SYSTEMCATEGORIES_REGEX], --systemcategories-regex [SYSTEMCATEGORIES_REGEX]
                        Regex for matching device display names in given
                        device group ID.
  -t [SYSTEMCATEGORIES_CSV], --systemcategories-csv [SYSTEMCATEGORIES_CSV]
                        Map of properties and values to assign to devices
                        matching system category regex.
  -m, --both-regex      Flag enabling additional property assignment if
                        device(s) match both regex
  -n [BOTH_CSV], --both-csv [BOTH_CSV]
                        Map of properties and values to assign to devices
                        matching both display name and system category regex.
  -l [LOG], --log [LOG]
                        Log path for saving script execution information.

"""

import argparse
import logging
import sys
import os
import csv
import re
from logic_monitor_client import LogicMonitorClient

FILENAME = os.path.basename(sys.argv[0])
LOGPATH = '../../logs/' + FILENAME.replace('.py', '.log')


def set_device_properties_using_property_list(logicmonitor_client, devices, property_list):
    """
    Use property list retrieved from CSV to assign properties to
    a filtered list of LogicMonitor device objects.

    :return: list of devices that were updated
    """
    updated_devices = []
    for device in devices:
        if(property_list):
            response = logicmonitor_client.set_device_custom_properties(device['id'], property_list)
            # The client would have returned None if the request were anything
            # but 200 (Success)
            if(response):
                logging.info('Updated properties on device ID {0}:{1}'.format(device['id'], device['displayName']))
                updated_devices.append(device)
        else:
            logging.info('No devices will be updated as property list is empty.')
    return updated_devices


def filter_in_devices_where_displayname_matches_regex(devices, device_property='displayName', regex_pattern='.*'):
    """
    Assign custom properties to LogicMonitor devices
    if they match a displayname or categories regex.
    Expect device_property to be 'displayName'.

    :return: list of LogicMonitor device objects matching displayname regex
    """
    matching_devices = []
    regex = re.compile(regex_pattern)
    logging.info('Matching device property {0} with pattern {1}'.format(device_property, regex_pattern))
    for device in devices:
        if(regex.match(device[device_property])):
            #assign properties
            logging.info('Pattern match on device ID:{0} displayName:{1} property:{2} value:{3}'.format(device['id'], device['displayName'], device_property, device[device_property]))
            matching_devices.append(device)

    return matching_devices


def filter_in_devices_where_categories_matches_regex(logicmonitor_client, devices, device_property='system.categories', regex_pattern='.*'):
    """
    Assign custom properties to LogicMonitor devices
    if they match a displayname or categories regex.
    Expect device_property to be 'categories'.
    'categories'.

    :return: list of LogicMonitor device objects matching displayname regex
    """
    matching_devices = []
    regex = re.compile(regex_pattern)
    logging.info('Matching property {0} with pattern {1}'.format(device_property, regex_pattern))
    for device in devices:
        property_value = None
        response = logicmonitor_client.get_device_property(device['id'], device_property)
        if(response):
            property_value = response['data']['value']
            if(regex.match(property_value)):
                logging.info('Pattern match on device ID:{0} displayName:{1} property:{2} value:{3}'.format(device['id'], device['displayName'], device_property, property_value))
                matching_devices.append(device)

    return matching_devices


def get_property_list_from_csv(filepath, property_header='name', value_header='value'):
    """
    Open a CSV file and parse its LogicMonitor device properties.
    In order to create the correct list of dictionaries needed for
    LogicMonitor's REST API PATCH of device custom properties,
    we must use the property_header of 'name' and value_header
    of 'value'.

    :return: list of device property-value pairs in dict form
    """
    property_list = []
    if(os.path.isfile(filepath)):
        with open(filepath) as csv_file:
            dict_reader = csv.DictReader(csv_file, delimiter=',',
                                         fieldnames=[property_header, value_header])
            for row in dict_reader:
                property_list.append(row)
    else:
        logging.info('Property list will be empty as CSV file not found:{0}'.format(filepath))
    return property_list


def main():
    """
    Main method
    :return: None
    """
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--company-name', type=str,
                            help='LogicMonitor customer portal.', required=True)
        parser.add_argument('-u', '--access-id', type=str,
                            help='LogicMonitor API token access ID.', required=True)
        parser.add_argument('-p', '--api-key', type=str,
                            help='LogicMonitor API token access key.', required=True)
        parser.add_argument('-g', '--device-groupid', type=str,
                            help='LogicMonitor device group ID to search for devices.', required=True)
        parser.add_argument('-d', '--systemdisplayname-regex', type=str, nargs='?',
                            help='Regex for matching device display names in given device group ID.',
                            required=False, default='')
        parser.add_argument('-e', '--systemdisplayname-csv', type=str, nargs='?',
                            help='Map of properties and values to assign to devices matching display name regex.',
                            required=False, default='')
        parser.add_argument('-s', '--systemcategories-regex', type=str, nargs='?',
                            help='Regex for matching device display names in given device group ID.',
                            required=False, default='')
        parser.add_argument('-t', '--systemcategories-csv', type=str, nargs='?',
                            help='Map of properties and values to assign to devices matching system category regex.',
                            required=False, default='')
        parser.add_argument('-m', '--both-regex', action='store_true',
                            help='Flag enabling additional property assignment if device(s) match both regex',
                            required=False, default=False)
        parser.add_argument('-n', '--both-csv', type=str, nargs='?',
                            help='Map of properties and values to assign to devices matching both display name and system category regex.',
                            required=False, default='')
        parser.add_argument('-l', '--log', type=str, nargs='?', const=LOGPATH,
                            help='Log path for saving script execution information.', required=True)
        args = parser.parse_args()

        logging.basicConfig(filename=args.log,
                            level=logging.INFO,
                            format='%(levelname)s:%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger().addHandler(logging.StreamHandler())
        logging.info('Executing {0} to assign LogicMonitor device custom properties in portal {1}.'.format(FILENAME, args.company_name))

        # Get properties from CSV for LogicMonitor devices matching
        # system.displayname
        displayname_property_list = get_property_list_from_csv(args.systemdisplayname_csv)

        # Get properties from CSV for LogicMonitor devices matching
        # system.categories
        categories_property_list = get_property_list_from_csv(args.systemcategories_csv)

        # Get properties from CSV for LogicMonitor devices matching both
        # system.displayname and system.categories
        both_property_list = get_property_list_from_csv(args.both_csv)

        # Get device group from LogicMonitor portal based on device group ID
        lm = LogicMonitorClient(args.company_name, args.access_id, args.api_key)
        logging.info("Getting LogicMonitor devices in device group id:{0}".format(args.device_groupid))
        response = lm.get_devices_by_group_id(args.device_groupid)
        devices = None
        if response['status'] == 200:
            logging.info('Device retrieval: Success')
            devices = response['data']['items']
        else:
            logging.info('Device retrieval: Failure')
            return

        devices_matching_displayname = []
        if(args.systemdisplayname_regex):
            # Create list of devices matching a regex for value of device
            # property "system.displayname"
            devices_matching_displayname = filter_in_devices_where_displayname_matches_regex(devices,
                                                                                            'displayName',
                                                                                            args.systemdisplayname_regex)

            # Set properties for devices that matched "system.displayname"
            response = set_device_properties_using_property_list(lm,
                                                                 devices_matching_displayname,
                                                                 displayname_property_list)

        devices_matching_categories = []
        if(args.systemcategories_regex):
            # Create list of devices matching a regex for value of device
            # property "system.categories"
            devices_matching_categories = filter_in_devices_where_categories_matches_regex(lm,
                                                                                           devices,
                                                                                           'system.categories',
                                                                                           args.systemcategories_regex)

            # Set properties for devices that matched "system.categories"
            response = set_device_properties_using_property_list(lm,
                                                                 devices_matching_categories,
                                                                 categories_property_list)
        
        devices_matching_both = []
        if(args.both_regex):
            # Find which devices match regex for both display name and system
            # categories. The technique used for finding this intersection is
            # called a Python List Comprehension. A Python set.intersection
            # could not be used because each LogicMonitor device is
            # represented as a mutable dictionary. Reference:
            # https://docs.python.org/2/tutorial/datastructures.html#list-comprehensions
            devices_matching_both = [device for device in devices_matching_categories if device in devices_matching_displayname]
            response = set_device_properties_using_property_list(lm,
                                                                 devices_matching_both,
                                                                 both_property_list)

        logging.info('Completed {0} execution.'.format(FILENAME))
        return
    except Exception, e:
        error_msg = 'Exception:' + str(e) + ' Message:' + e.message
        logging.error(error_msg)
        logging.info('Failed to complete {0} execution.'.format(FILENAME))
        return

if __name__ == '__main__':
    main()
