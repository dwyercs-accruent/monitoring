"""
This script is used for getting all alerts in a given time frame using the 
following filters: alert type, group path, name, logicmodule name, and
instance name. This includes that have cleared in the specified time
range. The alerts are written to a csv file specified.

example:
python GetPortalAlerts.py -c "accruent"
                          -u "tokenId" 
                          -p "tokenKey"
                          -t "device"
                          -g "Admin/Collectors*" 
                          -n "Four*"
                          -m "*"
                          -i "*Accruent*"
                          -s "07/19/17 13:00"
                          -e "07/19/17 14:00"
                          -v "../../output/GetPortalAlerts.csv"
                          -l "../../logs/GetPortalAlerts.log"
                            
usage: get_portal_alerts.py [-h] -c COMPANY_NAME -u ACCESS_ID -p API_KEY -t
                            ALERT_TYPE -g GROUP_FILTER -n NAME_FILTER -m
                            LOGICMODULE_FILTER -i INSTANCE_FILTER -s
                            START_TIME -e END_TIME -v CSV -l [LOG]

optional arguments:
  -h, --help            show this help message and exit
  -c COMPANY_NAME, --company-name COMPANY_NAME
                        LogicMonitor customer portal.
  -u ACCESS_ID, --access-id ACCESS_ID
                        LogicMonitor API token access id.
  -p API_KEY, --api-key API_KEY
                        LogicMonitor API token access key.
  -t ALERT_TYPE, --alert-type ALERT_TYPE
                        LogicMonitor alert type [*|service|device]
  -g GROUP_FILTER, --group-filter GROUP_FILTER
                        LogicMonitor device or service group path inclusion
                        filter.
  -n NAME_FILTER, --name-filter NAME_FILTER
                        LogicMonitor device or service name filter.
  -m LOGICMODULE_FILTER, --logicmodule-filter LOGICMODULE_FILTER
                        LogicMonitor LogicModule inclusion filter
  -i INSTANCE_FILTER, --instance-filter INSTANCE_FILTER
                        LogicMonitor instance (of LogicModule) inclusion
                        filter.
  -s START_TIME, --start-time START_TIME
                        Starting datetime for time window of included alerts
                        in "mm/dd/yy HH:MM" format.
  -e END_TIME, --end-time END_TIME
                        Ending datetime for time window of included alerts in
                        "mm/dd/yy HH:MM" format.
  -v CSV, --csv CSV     CSV filepath for saving alert data.
  -l [LOG], --log [LOG]
                        Log path for saving script execution information.


Output (unless overridden by user parameters):
../../output/get_portal_alerts.csv
../../logs/get_portal_alerts.log
"""
import argparse
import csv
import logging
import sys
import os
from logic_monitor_client import LogicMonitorClient, EpochConverter

FILENAME = os.path.basename(sys.argv[0])
LOGPATH = '../../logs/' + FILENAME.replace('.py', '.log')


def execute(company, access_id, access_key, alert_type, group_filter,
            name_filter, logicmodule_filter, instance_filter,
            start_datetime_str, end_datetime_str, csv_path):
    """
    Gets all alerts by filter and writes to csv.
    :param company: portal name
    :param access_id: token access id
    :param access_key: token access key
    :param alert_type: type of alert [*|service|device]
    :param group_filter: string filter for specifying device/service group
    :param name_filter: string filter for specifying device/service
    :param logicmodule_filter: string filter for specifying logicmodule
    :param instance_filter: string filter for specifying instance 
    :param start_datetime_str: string datetime in "mm/dd/yy HH:MM" format
    :param end_datetime_str: string datetime in "mm/dd/yy HH:MM" format
    :param csv_path: path to write to csv
    :return: 
    """
    logic_monitor_client = LogicMonitorClient(company, access_id, access_key)

    start_datetime_epoch = EpochConverter.convert_to_epoch(start_datetime_str)
    end_datetime_epoch = EpochConverter.convert_to_epoch(end_datetime_str)

    alerts = logic_monitor_client.get_group_alerts(alert_type,
                                                   group_filter,
                                                   name_filter,
                                                   logicmodule_filter,
                                                   instance_filter,
                                                   start_datetime_epoch,
                                                   end_datetime_epoch)
    for alert in alerts:
        alert['alertValue'] = alert['alertValue'].replace('\n', '')

    with open(csv_path, 'wb') as f:
        w = csv.DictWriter(f, alerts[0].keys(), quoting=csv.QUOTE_ALL)
        w.writeheader()
        for alert in alerts:
            w.writerow(alert)

    logging.info('Total alerts written to CSV={0}'.format(len(alerts)))


def main():
    """
    Main method
    :return: None
    """
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--company-name', type=str,
                            help='LogicMonitor customer portal.', required=True)
        parser.add_argument('-u', '--access-id', type=str,
                            help='LogicMonitor API token access id.', required=True)
        parser.add_argument('-p', '--api-key', type=str,
                            help='LogicMonitor API token access key.', required=True)
        parser.add_argument('-t', '--alert-type', type=str,
                            help='LogicMonitor alert type [*|service|device]', required=True)
        parser.add_argument('-g', '--group-filter', type=str,
                            help='LogicMonitor device or service group path inclusion filter.', required=True)
        parser.add_argument('-n', '--name-filter', type=str,
                            help='LogicMonitor device or service name filter.', required=True)
        parser.add_argument('-m', '--logicmodule-filter', type=str,
                            help='LogicMonitor LogicModule inclusion filter', required=True)
        parser.add_argument('-i', '--instance-filter', type=str,
                            help='LogicMonitor instance (of LogicModule) inclusion filter.', required=True)
        parser.add_argument('-s', '--start-time', type=str,
                            help='Starting datetime for time window of included alerts in "mm/dd/yy HH:MM" format.', required=True)
        parser.add_argument('-e', '--end-time', type=str,
                            help='Ending datetime for time window of included alerts in "mm/dd/yy HH:MM" format.', required=True)
        parser.add_argument('-v', '--csv', type=str,
                            help='CSV filepath for saving alert data.', required=True)
        parser.add_argument('-l', '--log', type=str, nargs= '?', const=LOGPATH,
                            help='Log path for saving script execution information.', required=True)
        args = parser.parse_args()

        logging.basicConfig(filename=args.log,
                            level=logging.INFO,
                            format='%(levelname)s:%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.getLogger("requests").setLevel(logging.WARNING)
        logging.getLogger().addHandler(logging.StreamHandler())
        logging.info('Executing {0} to get alerts...'.format(FILENAME))

        logging.info('Device/ServiceGroupFilter="{0}", '
                     'Device/ServerFilter="{1}", '
                     'DataSourceFilter="{2}", InstanceFilter="{3}", '
                     'StartTime="{4}", EndTime="{5}"'.
                     format(args.group_filter, args.name_filter,
                            args.logicmodule_filter, args.instance_filter,
                            args.start_time, args.end_time))

        execute(args.company_name, args.access_id, args.api_key,
                args.alert_type, args.group_filter, args.name_filter,
                args.logicmodule_filter, args.instance_filter, args.start_time,
                args.end_time, args.csv)
        logging.info('Completed {0} execution.'.format(FILENAME))
    except Exception, e:
        error_msg = 'Exception:' + str(e) + ' Message:' + e.message
        logging.error(error_msg)
        logging.info('Failed to complete {0} execution.'.format(FILENAME))


main()
