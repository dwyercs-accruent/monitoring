import base64
import hashlib
import hmac
import time
import datetime
import json
import urllib
import logging
import requests


class LogicMonitorClient:
    def __init__(self, company, access_id, access_key):
        """
        Constructor
        :param company: portal name
        :param access_id: token access id
        :param access_key: token access key
        """
        self.access_id = access_id
        self.access_key = access_key
        self.company = company

    def client_request(self, http_verb, resource_path, params='', data=''):
        """
        Generic client request method.
        :param http_verb: http verb
        :param resource_path: request end point
        :param params: filtering parameters
        :param data: payload for request
        :return: response json
        """
        http_verb = http_verb.upper()
        url = 'https://' + self.company + '.logicmonitor.com/santaba/rest' + \
              resource_path + params

        epoch = str(int(time.time() * 1000))
        request_vars = http_verb + epoch + data + resource_path
        signature = base64.b64encode(hmac.new(self.access_key, msg=request_vars,
                                              digestmod=hashlib.sha256).hexdigest())
        auth = 'LMv1 ' + self.access_id + ':' + signature + ':' + epoch
        headers = {'Content-Type': 'application/json', 'Authorization': auth}

        if http_verb == 'GET':
            response = requests.get(url, headers=headers)
        elif http_verb == 'POST':
            response = requests.post(url, data=data, headers=headers)
        elif http_verb == 'PUT':
            response = requests.put(url, data=data, headers=headers)
        elif http_verb == 'PATCH':
            response = requests.patch(url, data=data, headers=headers)
        elif http_verb == 'DELETE':
            response = requests.delete(url, headers=headers)

        if response.status_code == 200:
            return response.json()

    def get_datasources(self):
        datasources = []
        size = 1000
        resource_path = '/setting/datasources'
        response_dict = self.client_request('GET', resource_path,
                                            '?size=' + str(size) +
                                            '&offset=' + str(0))
        datasources.extend(response_dict['data']['items'])
        total = response_dict['data']['total']
        loops = (total / size) + 1
        for x in range(1, loops):
            response_dict = self.client_request('GET', resource_path,
                                                '?size=' + str(size) +
                                                '&offset=' + str(size * x))
            datasources.extend(response_dict['data']['items'])
        return datasources

    def get_devices_by_group_id(self, group_id):
        http_verb = 'GET'
        resource_path = '/device/groups/{id}/devices'.format(id=group_id)
        response = self.client_request(http_verb, resource_path)
        return response

    def get_device_property(self, device_id, property):
        http_verb = 'GET'
        resource = '/device/devices/{id}/properties/{property}'
        resource_path = resource.format(id=device_id, property=property)
        response = self.client_request(http_verb, resource_path)
        return response

    def get_device_logicmodules(self, object_id, logicmodule_filter):
        http_verb = 'GET'
        resource_path = '/device/devices/{0}/devicedatasources'.format(object_id)
        query_params = '?filter=dataSourceName~{0}'.format(logicmodule_filter)
        response = self.client_request(http_verb, resource_path, query_params)
        if len(response['data']['items']) > 0:
            return response['data']['items']

    def get_device_logicmodule_instances(self, object_id, logicmodule_id, instance_filter):
        http_verb = 'GET'
        resource_path = '/device/devices/{0}/devicedatasources/{1}/instances'.format(object_id, logicmodule_id)
        query_params = '?filter=displayName~{0}'.format(instance_filter)
        response = self.client_request(http_verb, resource_path, query_params)
        if len(response['data']['items']) > 0:
            return response['data']['items']

    def get_service_group(self, name):
        http_verb = 'GET'
        resource_path = '/service/groups'
        query_params = '?filter=name:{0}'.format(urllib.quote_plus(name))
        response = self.client_request(http_verb, resource_path, query_params)
        return response

    def get_group_alerts(self, alert_type, group_filter, name_filter,
                         logicmodule_filter, instance_filter, start_epoch,
                         end_epoch):
        """
        Gets all alerts for a specified device group and datasource within a
        specific time frame. This includes alerts that have cleared.
        :param alert_type: type of alert [service|device|*]
        :param group_filter: string filter for specifying device/service group
        :param name_filter: string filter for specifying device/service
        :param logicmodule_filter: string filter for specifying a datasource
        :param instance_filter: string filter for specifying a instance
        :param start_epoch: start time in UTC epoch
        :param end_epoch: start time in UTC epoch
        :return: alerts
        """
        alerts_dict = {}
        size = 1000
        resource_path = '/alert/alerts'

        # Parse alert type
        if alert_type == 'service':
            alert_type_filter = ',type:serviceAlert'
        elif alert_type == 'device':
            alert_type_filter = ',type!:serviceAlert'
        else:
            alert_type_filter = ',type:*'

        # Get alerts that began during the specified time range.
        params = '?size=' + str(size) + \
                 '&offset=' + str(0) + \
                 '&needMessage=true' + \
                 '&filter=cleared:*' + \
                 ',startEpoch>' + str(start_epoch) + \
                 ',startEpoch<' + str(end_epoch) + \
                 ',monitorObjectGroups:' + group_filter + \
                 ',monitorObjectName:' + name_filter + \
                 ',resourceTemplateName:' + logicmodule_filter + \
                 ',instanceName:' + instance_filter + \
                 alert_type_filter
        params = params.encode('utf-8')
        response_dict = self.client_request('GET', resource_path, params)
        logging.info('Response Code={0}, Response Message={1}'.
                     format(response_dict['status'], response_dict['errmsg']))
        self.__add_alerts_to_dict(response_dict['data']['items'], alerts_dict)
        search_id = response_dict['data']['searchId']
        total = len(response_dict['data']['items'])
        count = 1
        while total == size:
            params = '?size=' + str(size) + \
                     '&offset=' + str(size * count) + \
                     '&searchId=' + search_id + \
                     '&needMessage=true' + \
                     '&filter=cleared:*' + \
                     ',startEpoch>' + str(start_epoch) + \
                     ',startEpoch<' + str(end_epoch) + \
                     ',endEpoch<' + str(start_epoch) + \
                     ',endEpoch>' + str(end_epoch) + \
                     ',monitorObjectGroups:' + group_filter + \
                     ',monitorObjectName:' + name_filter + \
                     ',resourceTemplateName:' + logicmodule_filter + \
                     ',instanceName:' + instance_filter + \
                     alert_type_filter
            params = params.encode('utf-8')
            response_dict = self.client_request('GET', resource_path, params)
            logging.info('Response Code={0}, Response Message={1}'.
                         format(response_dict['status'], response_dict['errmsg']))
            total = len(response_dict['data']['items'])
            self.__add_alerts_to_dict(response_dict['data']['items'],
                                      alerts_dict)
            count += 1

        # Get alerts that ended during the specified time range.
        params = '?size=' + str(size) + \
                 '&offset=' + str(0) + \
                 '&needMessage=true' + \
                 '&filter=cleared:*' + \
                 ',endEpoch>' + str(start_epoch) + \
                 ',endEpoch<' + str(end_epoch) + \
                 ',monitorObjectGroups:' + group_filter + \
                 ',monitorObjectName:' + name_filter + \
                 ',resourceTemplateName:' + logicmodule_filter + \
                 ',instanceName:' + instance_filter + \
                 alert_type_filter
        params = params.encode('utf-8')
        response_dict = self.client_request('GET', resource_path, params)
        logging.info('Response Code={0}, Response Message={1}'.
                     format(response_dict['status'], response_dict['errmsg']))
        self.__add_alerts_to_dict(response_dict['data']['items'], alerts_dict)
        search_id = response_dict['data']['searchId']
        total = len(response_dict['data']['items'])
        count = 1
        while total == size:
            params = '?size=' + str(size) + \
                     '&offset=' + str(size * count) + \
                     '&searchId=' + search_id + \
                     '&needMessage=true' + \
                     '&filter=cleared:*' + \
                     ',endEpoch>' + str(start_epoch) + \
                     ',endEpoch<' + str(end_epoch) + \
                     ',monitorObjectGroups:' + group_filter + \
                     ',monitorObjectName:' + name_filter + \
                     ',resourceTemplateName:' + logicmodule_filter + \
                     ',instanceName:' + instance_filter + \
                     alert_type_filter
            params = params.encode('utf-8')
            response_dict = self.client_request('GET', resource_path, params)
            logging.info('Response Code={0}, Response Message={1}'.
                         format(response_dict['status'], response_dict['errmsg']))
            total = len(response_dict['data']['items'])
            self.__add_alerts_to_dict(response_dict['data']['items'],
                                      alerts_dict)
            count += 1

        if len(alerts_dict) > 0:
            return alerts_dict.values()
        else:
            return None

    def __add_alerts_to_dict(self, alerts, alerts_dict):
        for alert in alerts:
            alerts_dict[alert['id']] = alert
        return alerts_dict

    def set_device_custom_properties(self, device_id, properties):
        http_verb = 'PATCH'
        resource_path = '/device/devices/{0}'.format(device_id)
        query_params = '?patchFields=customProperties&opType=replace'
        data = {'customProperties':properties}

        #Format the string containing properties as expected by API
        #https://www.logicmonitor.com/support/rest-api-developers-guide/devices/update-a-device/#PATCH
        formatted_data = str(data).replace("'",'"')
        response = self.client_request(http_verb, resource_path, query_params, formatted_data)
        return response

    def set_device_group_sdt(self, device_group_id, start_time, stop_time,
                             comment):
        http_verb = 'POST'
        resource_path = '/sdt/sdts'
        json_comment = json.dumps(comment)
        unformatted_data = """{{\
                           "sdtType":1,\
                           "type":"DeviceGroupSDT",\
                           "deviceGroupId":"{0}",\
                           "startDateTime":"{1}",\
                           "endDateTime":"{2}",\
                           "comment":{3}\
                           }}"""
        data = unformatted_data.format(device_group_id, start_time, stop_time, json_comment)
        response = self.client_request(http_verb, resource_path, '?', data)
        return response

    def set_service_group_sdt(self, service_group_id, start_time, stop_time,
                              comment):
        http_verb = 'POST'
        resource_path = '/sdt/sdts'
        json_comment = json.dumps(comment)
        unformatted_data = """{{\
                           "sdtType":1,\
                           "type":"ServiceGroupSDT",\
                           "serviceGroupId":"{0}",\
                           "startDateTime":"{1}",\
                           "endDateTime":"{2}",\
                           "comment":{3}\
                           }}"""
        data = unformatted_data.format(service_group_id, start_time, stop_time, json_comment)
        response = self.client_request(http_verb, resource_path, '?', data)
        return response

    def set_device_logicmodule_sdt(self, object_id, logicmodule_id, start_time,
                                   stop_time, comment):
        http_verb = 'POST'
        resource_path = '/sdt/sdts'
        json_comment = json.dumps(comment)
        unformatted_data = """{{\
                           "sdtType":1,\
                           "type":"DeviceDataSourceSDT",\
                           "deviceId":"{0}",\
                           "deviceDataSourceId":"{1}",\
                           "startDateTime":"{2}",\
                           "endDateTime":"{3}",\
                           "comment":{4}\
                           }}"""
        data = unformatted_data.format(object_id, logicmodule_id, start_time,
                                       stop_time, json_comment)
        response = self.client_request(http_verb, resource_path, '?', data)
        return response

    def set_device_logicmodule_instance_sdt(self, object_id, instance_id,
                                            start_time, stop_time, comment):
        http_verb = 'POST'
        resource_path = '/sdt/sdts'
        json_comment = json.dumps(comment)
        unformatted_data = """{{\
                           "sdtType":1,\
                           "type":"DeviceDataSourceInstanceSDT",\
                           "deviceId":"{0}",\
                           "dataSourceInstanceId":"{1}",\
                           "startDateTime":"{2}",\
                           "endDateTime":"{3}",\
                           "comment":{4}\
                           }}"""
        data = unformatted_data.format(object_id, instance_id, start_time,
                                       stop_time, json_comment)
        response = self.client_request(http_verb, resource_path, '?', data)
        return response


class EpochConverter():
    @staticmethod
    def convert_to_epoch(datetime_str):
        """
        Converts string datetime to epoch time
        :param datetime_str: string date time in MM/DD/YY HH:MM format
        :return: epoch time
        """
        date_format = "%m/%d/%y %H:%M"
        epoch = datetime.datetime.utcfromtimestamp(0)
        start_datetime = datetime.datetime.strptime(datetime_str, date_format)
        return int((start_datetime - epoch).total_seconds())