# LogicMonitor - Professional Services

## Accruent

#### About

These scripts has been developed by Professional Services. It is provided to
customers on an as-needed basis and does not include support from
LogicMonitor. It is expected that customers using this script do so at their
own will, with the assumption that they have the tools, resources and knowledge
to run this script.

This script *may* use Unpublished API calls as well as our documented [REST
API calls](https://www.logicmonitor.com/support/rest-api-developers-guide/).
If unpublished API calls change in a future LogicMonitor release, it may affect
the behavior of this script. This script has been tested & verified with the
LogicMonitor version below.

Please let your Customer Success Manager (CSM) know if you would like to
engage Professional Services to enhance, modify or fix this script.

#### Versions

*   LogicMonitor tested & verified version: **v91**
*   Scripts: **v1.0.0**

#### Requirements

*   Python 2.7

#### Pip Packages
*   base64
*   hashlib
*   hmac
*   time
*   datetime
*   json
*   urllib
*   logging
*   requests
*   argparse
*   sys
*   os
*   csv
*   re

#### Scripts

*   **assign_custom_properties.py** - This script adds custom properties to 
devices in a device group via LogicMonitor's REST API. The custom properties 
must be listed in a CSV file where each new line contains a comma delimited
key value pair where the key is the property name and value is the property 
value: key1,value1
*   **create_logicmonitor_sdt.py** - This script creates either a device group or 
service group scheduled down time (SDT) via LogicMonitor's REST API.
*   **create_sdt_granular.py** - This script creates either a device, device 
datasource, or device datasource instance scheduled down time (SDT) via 
LogicMonitor's REST API.
*   **get_portal_alerts.py** - This script is used for getting all alerts in a 
given time frame using the following filters: alert type, group path, name, 
logicmodule name, and instance name. This includes that have cleared in the 
specified time range. The alerts are written to a csv file specified.
