﻿<#
--------------------------------------------------------------------------------------------------
v2 - adaptive service group sdt setting script

provide patterns to identify services, and loop through to SDT
  them for the window you specify. Must specify product stack in the pattern due to the
  standardized and therefore duplicate nature of each stack. This safeguards against mistakes.

method:
1. set variables to meet your patterns for stack, services, start+end times.
1.1 if multiples are needed af the patterns, then pipe "|" delimit them (see example that sdt's 
    vxField load balancer pools)
1.2 times should be long format "mm/dd/yyyy HH:MM:SS"
2. run the whole script with the play button in ISE.
3. if you have failed to meet any variable requirements you'll be prompted for them
3.1 just cancel the run and make sure you fill in the variables, the way this loops you'll be
    prompted to death.

--------------------------------------------------------------------------------------------------
author: Cory Dwyer
date: 12/17/2017
contact: ribbonsalad@accruent.com
--------------------------------------------------------------------------------------------------
#>

<#////////////////////////////////////////////////////////////////////////////////////////////////

VARIABLE SUBSECTION

Assign values to these within the provided quotes whether single or double. It does matter which
    are used for a field so do not swap them. (as of 12/15/2017)

$apiId - assign your api ID string.
$apiKey - assign your api security key.
$company - 'accruent'   <... this one is static eh?

$svcGrpPattern - assign the service group path. MUST use whole path. 
                 pipe(|) delimit multiple paths

$startTime
$stopTime
 ^
 |
 ---- assign both of these using a string like this "01/31/2021 16:12:00"
      set time according to your local system time zone where you run this script
      the script will adjust for UTC automatically.

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#>

$apiId = ''
$apiKey = ''
$company = 'accruent'

$svcGrpPattern = ""

$StartTime = "12/31/2017 23:00:00"
$EndTime = "12/31/2017 23:30:00"

<#////////////////////////////////////////////////////////////////////////////////////////////////

DATA GATHERING FUNCTIONS

get-lmservicegroup :
 - supply the path (what things show up as in the tree or the tile bar)
 Most frequently this is used to gather the group ID.
  this is true in the use case for this script.

 You may use these in the powershell/command pane in ISE once they have been loaded once and
 manually poll the API for the information you need to fill in the variables in the script.
   Just set up your apiId and apiKey variables so you can use them as pipeline parameters so
   you don't have to type them out all the time.

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#>

function get-lmservicegroup {
            param(
                [parameter(mandatory=$true)]
                $grpPath,
                [parameter(mandatory=$true)]
                $accessId,
                [parameter(mandatory=$true)]
                $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = '/service/groups'
  $queryParameters = "?filter=fullPath:$grpPath"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmservicegroup = $response
}

<#////////////////////////////////////////////////////////////////////////////////////////////////

SDT POSTING FUNCTION 

set-ServiceSdt : this takes your api creds, the service IDs gathered, and sets an SDT that starts
                 and stops in UTC (converted from the local time you entered)

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#>

Function set-SvcGroupSdt {
                    Param(
                   [parameter(mandatory=$true)]
                    $accessId,
                   [parameter(mandatory=$true)]
                    $accessKey,
                   [parameter(mandatory=$true)]
                    $serviceGroupId,
                   [parameter(mandatory=$true)]
                    $startDate,
                   [parameter(mandatory=$true)]
                    $endDate
                    )
  <# start with time math for later #>
  $origin = New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0
  $epochconv = (get-date -Date $startDate).ToUniversalTime().Subtract($origin)
  $startDateTime = [math]::Round($epochconv.TotalMilliseconds)
  $epochconv = (get-date -Date $endDate).ToUniversalTime().Subtract($origin)
  $endDateTime = [math]::Round($epochconv.TotalMilliseconds)
  <# request details #>
  $httpVerb = 'POST'
  $resourcePath = '/sdt/sdts'
  $json = @{
    sdtType=1
    type="ServiceGroupSDT"
    serviceGroupId=$serviceGroupId
    startDateTime=$startDateTime
    endDateTime=$endDateTime
  }
  $data = $json | ConvertTo-Json
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $data + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method $httpVerb -Body $data -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json -Depth 5
  Write-Host "Status:$status"
  Write-Host "Response:$body"
}

<#////////////////////////////////////////////////////////////////////////////////////////////////

LOGIC SUBSECTION

get service groups that match the given pattern

for each service group that matches the service pattern(s) provided
  
  set an SDT of timespan provided, converted to UTC from local, to the current service id

////////////////////////////////////////////////////////////////////////////////////////////////#>

get-lmservicegroup $svcGrpPattern $apiId $apiKey
$svcgrpIds = $lmservicegroup.data.items
foreach ($svcgrpId in $svcgrpIds) {
  set-SvcGroupSdt $apiId $apiKey $svcgrpId.id $StartTime $EndTime
}
