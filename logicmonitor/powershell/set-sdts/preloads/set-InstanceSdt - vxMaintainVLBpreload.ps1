﻿<#
--------------------------------------------------------------------------------------------------
v2 - adaptive instance sdt setting script

provide patterns to identify instances<(of)<datasources<(on)<devices, and loop through to SDT
  them for the window you specify.

method:
1. set variables to meet your patterns for devices, datasources, and instances, start+end times.
1.1 if multiples are needed af the patterns, then pipe "|" delimit them (see example that sdt's 
    vxField load balancer pools)
1.2 times should be long format "mm/dd/yyyy HH:MM:SS"
2. run the whole script with the play button in ISE.
3. if you have failed to meet any variable requirements you'll be prompted for them
3.1 just cancel the run and make sure you fill in the variables, the way this loops you'll be
    prompted to death.

--------------------------------------------------------------------------------------------------
author: Cory Dwyer
date: 12/15/2017
contact: ribbonsalad@accruent.com
--------------------------------------------------------------------------------------------------
#>

<#////////////////////////////////////////////////////////////////////////////////////////////////

VARIABLE SUBSECTION

Assign values to these within the provided quotes whether single or double. It does matter which
    are used for a field so do not swap them. (as of 12/15/2017)

$apiId - assign your api ID string.
$apiKey - assign your api security key.
$company - 'accruent'   <... this one is static eh?

$devicePattern - assign the device display name pattern that has the datasource pattern which...
$datasourcePattern - you will assign to this variable. This datasource pattern should...
$instancePattern - contain instances that match the instance pattern you provide in this variable

$startTime
$stopTime
 ^
 |
 ---- assign both of these using a string like this "01/31/2021 16:12:00"
      set time according to your local system time zone where you run this script
      the script will adjust for UTC automatically.

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#>

$apiId = ''
$apiKey = ''
$company = 'accruent'

$devicePattern = "vlb01|vlb02"
$datasourcePattern = "pool"
$instancePattern = "VERI*prd*eam|COMMON*prd*app"

$StartTime = "01/03/2018 15:00:00"
$EndTime = "01/03/2018 20:15:00"

<#////////////////////////////////////////////////////////////////////////////////////////////////

DATA GATHERING FUNCTIONS

get-lmdevice :
 - supply the display name (what things show up as in the tree or the tile bar)
 Most frequently this is used to gather the device ID.
  this is true in the use case for this script.

get-lmdevdatasource :
 - supply datasource name pattern
 and
 - supply device id
 This is used to find the datasource ID in order to get the instance ID's next

 You may use these in the powershell/command pane in ISE once they have been loaded once and
 manually poll the API for the information you need to fill in the variables in the script.
   Just set up your apiId and apiKey variables so you can use them as pipeline parameters so
   you don't have to type them out all the time.

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#>

function get-lmdevice {
            param(
                [parameter(mandatory=$true)]
                $displayName,
                [parameter(mandatory=$true)]
                $accessId,
                [parameter(mandatory=$true)]
                $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = '/device/devices'
  $queryParameters = "?filter=displayName~$displayName"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdevice = $response
}

function get-lmdevdatasource {
                    param(
                        [parameter(mandatory=$true)]
                        $name,
                        [parameter(mandatory=$true)]
                        $devId,
                        [parameter(mandatory=$true)]
                        $accessId,
                        [parameter(mandatory=$true)]
                        $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = "/device/devices/$devId/devicedatasources"
  $queryParameters = "?filter=dataSourceName~$name"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdevdatasource = $response
}

function get-lmdsinstances {
                    param(
                        [parameter(mandatory=$true)]
                        $name,
                        [parameter(mandatory=$true)]
                        $datasourceId,
                        [parameter(mandatory=$true)]
                        $deviceId,
                        [parameter(mandatory=$true)]
                        $accessId,
                        [parameter(mandatory=$true)]
                        $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = "/device/devices/$deviceId/devicedatasources/$datasourceId/instances/"
  $queryParameters = "?filter=name~$name"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdsinstances = $response
}

<#////////////////////////////////////////////////////////////////////////////////////////////////

SDT POSTING FUNCTION 

set-InstanceSdt : this takes your api creds, the device IDs gathered, and the instance IDs found
                  on those device IDs and sets an SDT that starts and stops in UTC (converted 
                  from the local time you entered)

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#>

Function set-InstanceSdt {
                    Param(
                   [parameter(mandatory=$true)]
                    $accessId,
                   [parameter(mandatory=$true)]
                    $accessKey,
                   [parameter(mandatory=$true)]
                    $deviceId,
                   [parameter(mandatory=$true)]
                    $instanceId,
                   [parameter(mandatory=$true)]
                    $startDate,
                   [parameter(mandatory=$true)]
                    $endDate
                    )
  <# start with time math for later #>
  $origin = New-Object -Type DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0
  $epochconv = (get-date -Date $startDate).ToUniversalTime().Subtract($origin)
  $startDateTime = [math]::Round($epochconv.TotalMilliseconds)
  $epochconv = (get-date -Date $endDate).ToUniversalTime().Subtract($origin)
  $endDateTime = [math]::Round($epochconv.TotalMilliseconds)
  <# request details #>
  $httpVerb = 'POST'
  $resourcePath = '/sdt/sdts'
  $json = @{
    sdtType=1
    type="DeviceDataSourceInstanceSDT"
    deviceId=$deviceId
    dataSourceInstanceId=$instanceId
    startDateTime=$startDateTime
    endDateTime=$endDateTime
  }
  $data = $json | ConvertTo-Json
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $data + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method $httpVerb -Body $data -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json -Depth 5
  Write-Host "Status:$status"
  Write-Host "Response:$body"
}

<#////////////////////////////////////////////////////////////////////////////////////////////////

LOGIC SUBSECTION

for each device that matches the device pattern(s) provided
  
  get the datasource id matching the pattern(s) provided
  
  for each datasource id in the matching ids
    
    get the instances that match the pattern(s) provided
    
    for each instance id in the matching ids
      
      set an SDT of timespan provided, converted to UTC from local, to the current instance id
      on the current device id.

////////////////////////////////////////////////////////////////////////////////////////////////#>

get-lmdevice $devicePattern $apiId $apiKey
$devIds = $lmdevice.data.items
foreach ($devId in $devIds) {
  get-lmdevdatasource $datasourcePattern $devId.id $apiId $apiKey
  $dsIds = $lmdevdatasource.data.items
  foreach ($dsId in $dsids) {
    get-lmdsinstances $instancePattern $dsId.id $devId.id $apiId $apiKey
    $dsinstIDs = $lmdsinstances.data.items
    foreach ($dsinstID in $dsinstIDs) {
      set-InstanceSdt $apiId $apiKey $devId.id $dsinstID.id $StartTime $EndTime
    }
  }
}
