﻿# vars for logstash shipping

#kibs
$kbdfp = "10.50.6.135"
$kbhex = "172.19.169.135"
#dfp
$lsdfp00 = "10.50.6.140"
$lsdfp01 = "10.50.6.141"
$lsdfp02 = "10.50.6.142"
#dfq
$lsdfq00 = "10.50.6.143"
$lsdfq01 = "10.50.6.144"
$lsdfq02 = "10.50.6.145"
#usi
$lsusi00 = "10.182.169.140"
$lsusi01 = "10.182.169.141"
$lsusi02 = "10.182.169.142"
#hex
$lshex00 = "172.19.169.140"
$lshex01 = "172.19.169.141"
$lshex02 = "172.19.169.142"
#pgt
$lspgt00 = "172.18.169.140"
$lspgt01 = "172.18.169.141"
$lspgt02 = "172.18.169.142"

$lsnodes = @($lsdfp00,$lsdfp01,$lsdfp02,$lsdfq00,$lsdfq01,$lsdfq02,$lsusi00,$lsusi01,$lsusi02,$lshex00,$lshex01,$lshex02,$lspgt00,$lspgt01,$lspgt02)
$lsport = 9525
$LogStashData = ""

# function to ship to logstash

Function ShipToLogstash {
                param (
                                [ValidateNotNullOrEmpty()]
                                [string] $server,
                                [int] $port,
                                $jsondata)
                $ip = $server
                $address = [System.Net.IPAddress]::Parse($ip)
                $socket = New-Object System.Net.Sockets.TCPClient($address, $port)
                $stream = $socket.GetStream()
                $writer = New-Object System.IO.StreamWriter($stream)
 
                $writer.WriteLine($jsondata)
                $writer.Flush()
 
                $stream.Close()
                $socket.Close()
}


# execution

foreach ($node in $lsnodes[0..2]) {
  $currenttime = Get-Date
  $LogStashData = ("The current time is: " + $currenttime + ". This is a test of logstash node " + $node)
  ShipToLogstash $node $lsport $LogStashData
}

ShipToLogstash $lsusi01 $lsport ("this is a test of logstash ingest @ " + $currenttime)