﻿$apiId = ''
$apiKey = ''
$company = 'accruent'

$devicePattern = "msppvc01"
$datasourcePattern = "ESXhost"
$instancePattern = "*"



function get-lmdevice {
            param(
                [parameter(mandatory=$true)]
                $displayName,
                [parameter(mandatory=$true)]
                $accessId,
                [parameter(mandatory=$true)]
                $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = '/device/devices'
  $queryParameters = "?filter=displayName~$displayName"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdevice = $response
}
function get-lmdevdatasource {
                    param(
                        [parameter(mandatory=$true)]
                        $name,
                        [parameter(mandatory=$true)]
                        $devId,
                        [parameter(mandatory=$true)]
                        $accessId,
                        [parameter(mandatory=$true)]
                        $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = "/device/devices/$devId/devicedatasources"
  $queryParameters = "?filter=dataSourceName~$name"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdevdatasource = $response
}
function get-lmdsinstances {
                    param(
                        [parameter(mandatory=$true)]
                        $name,
                        [parameter(mandatory=$true)]
                        $datasourceId,
                        [parameter(mandatory=$true)]
                        $deviceId,
                        [parameter(mandatory=$true)]
                        $accessId,
                        [parameter(mandatory=$true)]
                        $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = "/device/devices/$deviceId/devicedatasources/$datasourceId/instances/"
  $queryParameters = "?filter=name~$name"
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdsinstances = $response
}
function get-lmdevproperties {
            param(
                [parameter(mandatory=$true)]
                $deviceId,
                [parameter(mandatory=$true)]
                $accessId,
                [parameter(mandatory=$true)]
                $accessKey)
  <# request details #>
  $httpVerb = 'GET'
  <# linux group items #>
  $resourcePath = "/device/devices/$deviceId/properties"
  $queryParameters = ""
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParameters
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method Get -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json
  Write-Host "Status:$status"
  Write-Host "Response:$body"
  $Global:lmdevproperties = $response
}
