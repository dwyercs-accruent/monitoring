﻿
<# code to use for puts/posts/patches so we can use variables and not need single quotes #>
$json = @{
  parentId="$example"
  name="$example2"
}
$data = $json | ConvertTo-Json

$json = @{customProperties=@{
      name="$property"
      value="$value"
    }
  }
$data = $json | ConvertTo-Json
<#_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_#>



<# 
   code to patch a property on a device, must manually modify the property and value in the 
   data var within the function. Have not yet been able to get customProperties to work with
   convertto-json ...
#>

Function Patch-lmapidevproperty($devid){
  <# account info #>
  $accessId = ''
  $accessKey = ''
  $company = 'accruent'
  <# request details #>
  $httpVerb = 'PATCH'
  $resourcePath = "/device/devices/$devid"
  $data = $json | ConvertTo-Json
  $queryParams = '?patchFields=customProperties&opType=replace'
  $data = '{"customProperties":[{"name":"testprop","value":"shrodingerscat"}]}'
  <# Construct URL #>
  $url = 'https://' + $company + '.logicmonitor.com/santaba/rest' + $resourcePath + $queryParams
  <# Get current time in milliseconds #>
  $epoch = [Math]::Round((New-TimeSpan -start (Get-Date -Date "1/1/1970") -end (Get-Date).ToUniversalTime()).TotalMilliseconds)
  <# Concatenate Request Details #>
  $requestVars = $httpVerb + $epoch + $data + $resourcePath
  <# Construct Signature #>
  $hmac = New-Object System.Security.Cryptography.HMACSHA256
  $hmac.Key = [Text.Encoding]::UTF8.GetBytes($accessKey)
  $signatureBytes = $hmac.ComputeHash([Text.Encoding]::UTF8.GetBytes($requestVars))
  $signatureHex = [System.BitConverter]::ToString($signatureBytes) -replace '-'
  $signature = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($signatureHex.ToLower()))
  <# Construct Headers #>
  $auth = 'LMv1 ' + $accessId + ':' + $signature + ':' + $epoch
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.Add("Authorization",$auth)
  $headers.Add("Content-Type",'application/json')
  <# Make Request #>
  $response = Invoke-RestMethod -Uri $url -Method $httpVerb -Body $data -Header $headers 
  <# Print status and body of response #>
  $status = $response.status
  $body = $response.data| ConvertTo-Json -Depth 5
  Write-Host "Status:$status"
  Write-Host "Response:$body"
}



