How To Get API Access:

EMAIL monitoring@accruent.com

You will receive a reply with your API ID and Key within 24 hours.

- If you lose your key, you can email again to recover it or have a new one issued.

- treat it like a paper towel. These should be disposable, and will be rotated occasionally. You'll be emailed with a new one when they are rotated.


What Are These Chunks of Powershell:

code blocks for powershell api interaction. You will need to update the code with your api tokenID and tokenKey.

these return the results to ps objects for further interaction. These can be manipulated fairly easily to perform well in scripts to programmatically change many items with foreach loops.

API scripting should not be done without responsible testing prior to a live run. A loop gone wrong can wreck our LogicMonitor instance in a hurry. Furthermore, if it affects services there stands the possibility of it affecting prod by means of an accidental DDoS via monitoring.

    ask questions
    test thoroughly
    ask more questions
    test more thoroughly

API token access granted on a need / case by case basis.

Use PowerShell ISE and copy whichever functions you are using to your scripting pane. If your Execution policy settings are strict (which they usually are) you can just run the code as a snippet by selecting it all and running as a snippet.

Remember that you need to enter your api ID and Key into the respective variables, and update the start and end times if required for something like an SDT. 